class scripts::firewall::setup {
	file { '/etc/firewall':
		ensure  => directory,
      recurse => true,
      purge   => true,
		source  => 'puppet:///git/services/configuration/firewall/etc/firewall',
		owner   => root,
		group   => root,
	}

	file { '/etc/.firewall':
		replace => 'no',
		ensure  => 'directory',
		purge   => 'true',
		recurse => true,
		source  => 'puppet:///git/services/configuration/firewall/etc/.firewall',
		owner   => root,
		group   => root,
	}

	file { '/etc/init.d/firewall':
		ensure  => file,
		source  => 'puppet:///git/services/configuration/firewall/etc/init.d/firewall',
		owner   => root,
		group   => root,
		mode    => '744',
	}

	file { '/etc/init.d/flame-on':
		ensure  => file,
		source  => 'puppet:///git/services/configuration/firewall/etc/init.d/flame-on',
		owner   => root,
		group   => root,
		mode    => '744',
	}

	file { '/etc/init.d/firewall.stop':
		ensure  => file,
		source  => 'puppet:///git/services/configuration/firewall/etc/init.d/firewall.stop',
		owner   => root,
		group   => root,
		mode    => '744',
	}
	
	file { '/etc/rsyslog.d/firewall.conf':
		ensure  => file,
		source  => 'puppet:///git/services/configuration/firewall/etc/rsyslog.d/firewall.conf',
		owner   => root,
		group   => root,
	}

	file {'/usr/sbin/firewall':
		ensure => link,
		target => '/etc/firewall/firewall',
	}

	file {'/etc/firewall/ports-in':
		ensure => link,
		target => '/etc/.firewall/ports-in',
	}

	file {'/etc/firewall/ports-out':
      ensure => link,
      target => '/etc/.firewall/ports-out',
   }

	file {'/etc/firewall/send-to-devnull':
      ensure => link,
      target => '/etc/.firewall/send-to-devnull',
   }

	file {'/etc/firewall/whitelist':
      ensure => link,
      target => '/etc/.firewall/whitelist',
   }

	service { 'rsyslog':
		ensure     => running,
		hasstatus  => true,
		hasrestart => true,
		subscribe  => File['/etc/rsyslog.d/firewall.conf'],
	}

	file {'/var/log/firewall/': ensure => directory }
	file {'/var/log/firewall/firewall.log': ensure => file }
	file {'/var/log/firewall/firewall.log.action': ensure => file }
}
