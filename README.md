> Awesome Firewall Script (AFS xD) - Set of scripts to manage the default firewall on APIS nodes

###Basic usage:

There is one main script used to interface with iptables: /usr/sbin/firewall

```
[root@portal ~]# firewall --help
[Firewall]

firewall [ function/action ] [ options ]

***Options***

         firewall --profile [profile] ( load firewall profile )
         firewall --blacklist ban [ip] ( ban IP )
         firewall --whitelist [ip] ( whitelist IP )

         firewall check ( check firewall status )
         firewall restart ( restart firewall )
         firewall start ( start firewall )
         firewall stop ( stop firewall )
         firewall flush ( reset to defaults )
```

###Installation:

The installation is pretty straight forward.

```
git clone https://bitbucket.org/splodyxd/apis-firewall.git firewall
cd firewall
sudo bash install
```

###Configuration:

The 'root' user is only required to install, [re]start and stop the firewall. Via the installation method above, this script can be installed under a regular user. This user will have access to modify and change the configuration of the firewall (but not [re]start|stop it.)

- /usr/sbin/firewall: This is the iptables wrapper scripts (Acts as a pointer to /etc/firewall/firewall)

- /etc/firewall: Configuration files are kept here.

- /etc/.firewall: This folder acts as a permanent shadow configuration, this should not be deleted between reinstalls and upgrades.

- /etc/firewall/ports-in: Any ports that you wish to be opened INBOUND should go here, one per line.

- /etc/firewall/ports-out: Any ports that you wish to be opened OUTBOUND should go here, one per line.

- /etc/firewall/send-to-devnull: IPs in this file are dropped in the firewall (-j REJECT).

- /etc/firewall/whitelist: IPs in this file are indiscriminately whitelisted.

- /etc/.firewall/firewall.conf: Main firewall configuration

- /etc/.firewal/modules/includes: Custom firewall modules go here (eg. ../includes/mod.nas-rules, ../includes/mod.vpn-rules) [More modules can be found here: http://git.splodycode.net/apis-firewall-modules/src]. Modules in this folder are loaded in alphabetical order; keep this in mind when creating naming schemes (mod.allowONLYmyipaddress should probably be included before mod.BLOCKeveryone but with this naming convention everyone would get blocked first).

###Options:

firewall --profile [profile] ( load firewall profile )

- [Future feature]

firewall --blacklist ban [ip] ( ban IP )

- Adds IPs to the blacklist (/etc/firewall/send-to-devnull). 

- Example: firewall --blacklist ban 1.1.1.1

firewall --whitelist [ip] ( whitelist IP )

- Adds IPs to the whitelist (/etc/firewall/whitelist).

- Example: firewall --whitelist 1.1.1.1

firewall check ( check firewall status )

- List the current firewall rules (Looks pretty when piped to 'less -S')

firewall restart ( restart firewall )

- Restarts the firewall, loads the last used profile

firewall start ( start firewall )

- Starts the firewall with the last used profile

firewall stop ( stop firewall )

- Stops the firewall

firewall flush ( reset to defaults )

- Flushes the firewall back to defaults

###These scripts are experimental. It's highly recommended that you test any modifications of this script on a local machine that you have access to should you get locked out. Use these scripts at your own risk.
